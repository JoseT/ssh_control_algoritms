clc
clear all
%% Variables generales (no cambian)
s = tf('s')
Vs = 24;
Voee = 48;
Dee = (1-(Vs/Voee));
Ts = 1.6e-6;
L = 200e-6;
C = (2*220e-5);
Wc = 2*pi*1.5e3;
Kp_I = 0.025;
Ki_I = 18;

%% Región Ilee = 1.5 A
Ilee1 = 1.5;
R1 = Voee/((1-Dee)*Ilee1)
Kp1 = 2.39;
Ki1 = 154.6;
Gc = Kp1+(Ki1/s)

Ilee=Ilee1;
R=R1;
%% Región Ilee = 4.5 A
Ilee2 = 4.5;
R2 = Voee/((1-Dee)*Ilee2)
Kp2 = 2.26;
Ki2 = 190;
Gc = Kp2+(Ki2/s)

Ilee=Ilee2;
R=R2;
%% Región Ilee = 7.5 A
Ilee3 = 7.5;
R3 = Voee/((1-Dee)*Ilee3)
Kp3 = 2.13;
Ki3 = 210;
Gc = Kp3+(Ki3/s)

Ilee=Ilee3;
R=R3;
%% Región Ilee = 10.5 A
Ilee4 = 10.5;
R4 = Voee/((1-Dee)*Ilee4)
Kp4 = 2.06;
Ki4 = 230;
Gc = Kp4+(Ki4/s)

Ilee=Ilee4;
R=R4;
%% Región Ilee = 13.5 A
Ilee5 = 13.5;
R5 = Voee/((1-Dee)*Ilee5)
Kp5 = 2.06;
Ki5 = 280;
Gc = Kp5+(Ki5/s)

Ilee=Ilee5;
R=R5;
%% Transferencia de la matriz continua

Am = [0 -(1-Dee)/L (Voee*Ki_I)/L -(Voee*Kp_I)/L; (1-Dee)/C -1/(C*R) -(Ilee*Ki_I)/C (Ilee*Kp_I)/C; 0 0 0 -1; Wc 0 0 -Wc]
Bm = [(Voee*Kp_I)/L; -(Ilee*Kp_I)/C; 1; 0]
Cm = [0 1 0 0]
Dm = 0;
[num,dem] = ss2tf(Am,Bm,Cm,Dm)

Glazocorriente = tf(num,dem)
Glazocorriente = zpk(Glazocorriente)

pidTuner(Glazocorriente,Gc)
%pidTuner(Glazocorriente,'PI')

%%
format shortEng
KP = [Kp1 Kp2 Kp3 Kp4 Kp5];
KI = [Ki1 Ki2 Ki3 Ki4 Ki5];
IL_zonas = [1.5 4.5 7.5 10.5 13.5];
KP_coefs = polyfit(IL_zonas,KP,2)
KI_coefs = polyfit(IL_zonas,KI,2)

figure(1)
plot(IL_zonas,KP)
hold on
plot(IL_zonas,polyval(KP_coefs,IL_zonas))

figure(2)
plot(IL_zonas,KI)
hold on
plot(IL_zonas,polyval(KI_coefs,IL_zonas))
%% Lazo de corriente (original)
Ilee = 4.5;
R = Voee/((1-Dee)*Ilee);
Gc = 0.02+(12/s);

Fc = Wc/(s+Wc)

Gi_lc = ((Voee/L)*s + (((Ilee*(1-Dee))/(L*C)) + Voee/(L*C*R)))/(s^2+(s/(C*R))+((1-Dee)^2)/(L*C))
zpk(Gi_lc)

Fcz = c2d(Fc,Ts)
Giz_lc = c2d(Gi_lc,Ts)
%pidTuner(Gi_lc*Fc,Gc)
