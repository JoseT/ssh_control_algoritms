%% Analisis de auto valores de A

syms x De Li Voe Kii Kpi Ci Ri W Ile
A = [0 -(1-De)/Li (Voe*Kii)/Li -(Voe*Kpi)/Li;...
    (1-De)/Ci -1/(Ci*Ri) -(Ile*Kii)/Ci (Ile*Kpi)/Ci;...
    0 0 0 -1; W 0 0 -W]
I = eye(4)

p=det(A-I*x)


Vs = 24;
Ts = 5e-6;
Li = 200e-6;
Ci = 220e-5;
W = 2*pi*1.5e3;
Kpi = 0.027;
Kii = 37;
Voe = 48;
De = (1-(Vs/Voe));
Ri=Voe*(1-De)/Ile;

p_r = subs(p);
polo = [];
for i=0.1:0.05:16
p_r1 = subs(p_r,Ile,i);
p_coe = double(coeffs(p_r1,x));
polo = [polo;roots(p_coe)' i];
end
%%
figure(1)
plot(polo(:,5),real(polo(:,1)))

figure(2)
plot(polo(:,5),real(polo(:,2)))

figure(3)
plot(polo(:,5),real(polo(:,3)))

figure(4)
plot(polo(:,5),real(polo(:,4)))
%% Variacion de polos respecto a IL
figure(5)
plot(real(polo(:,1)),imag((polo(:,1))))
hold on
plot(real(polo(:,2)),imag((polo(:,2))),'r')
plot(real(polo(:,3)),imag((polo(:,3))),'m')
plot(real(polo(:,4)),imag((polo(:,4))),'b')

%% Sensibilidad de polos respecto a IL
p1 = real(polo(:,1));
p2 = real(polo(:,2));
p3 = real(polo(:,3));
p3i = imag(polo(:,3));
p4 = real(polo(:,4));
p4i = imag(polo(:,4));

iL = linspace(0.1,16,319);

%se verifica la adaptacion de la curva obtenida con los puntos reales del
%polo 1

a = -0.8414;
b = -6.787;
c = -0.07538;
d = -0.4123;
fp1 = a*exp(b*iL) + c*exp(d*iL);

figure(1)
plot(iL, fp1)
hold on
plot(iL,p1)

sp1 = diff(fp1)
plot(iL,[sp1 0])


coefs_polo2 = polyfit(iL,p2,2);
coefs_polo3 = polyfit(iL,p3,2);
coefs_polo3i = polyfit(iL,p3i,2);
coefs_polo4 = polyfit(iL,p4,2);
coefs_polo4i = polyfit(iL,p4i,2);

figure(2)
plot(iL,p2)
hold on
plot(iL,polyval(coefs_polo2,iL))

figure(3)
plot(iL,p3)
hold on
plot(iL,polyval(coefs_polo3,iL))

figure(4)
plot(iL,p3i)
hold on
plot(iL,polyval(coefs_polo3i,iL))

figure(5)
plot(iL,p4)
hold on
plot(iL,polyval(coefs_polo4,iL))

figure(6)
plot(iL,p4i)
hold on
plot(iL,polyval(coefs_polo4i,iL))

sp2 = diff(coefs_polo2);
sp3 = diff(coefs_polo3);
sp3i = diff(coefs_polo3i);
sp4 = diff(coefs_polo4);
sp4i = diff(coefs_polo4i);

sTotal=sqrt([sp1 sp1(end)].^2+polyval(sqrt((sp2+sp3+sp4).^2+(sp3i+sp4i).^2),iL).^2)
figure(8)
plot(iL,sTotal)